# docker-chrome-node-cypress

Dockerimage with latest chrome & latest node. May be used for cypress testing or as general purpose node image.

## Why?
I build an app with Node/TS backend and a frontend and automated some stuff in a gitlab pipeline, eg. testing, building, regular dependency updates.....

The frontend tests are built with cypress and run in headless chrome. To be able to run all that stuff in a (gitlab) pipeline, I needed an appropriate docker image and nothing I found on dockerhub fit 100%.
There was always some dependency missing or outdated versions (I use latest node, because the app is just for fun) or some other shit that started to annoy me.

So I decided to just build my own custom docker image and everything is fine. I also wanted to showcase how easy it is to build a custom docker image and keep it up-to-date automatically with gitlab.

## How?
How is gitlab making it super easy and fun to do that? Despite being a lot faster than trying different images from dockerhub.
1. Automation is easy through pipelines, just execute whatever you would in cmd
2. Gitlab Pipelines work with docker out of the box (even docker-in-docker)
3. Just write a Dockerfile with everything you need
4. Build and Tag it in the pipeline
5. Push it to the gitlab docker registry (it's a part of every repo you create)
6. If the repo is public, so is the docker registry -> you can use it everywhere, just like every other dockerhub image
8. :heart: GitLab

_Of course you could push the image to dockerhub in the pipeline, but I didn't see a reason to do that._

## Alternatives
There are alternatives on docker hub, but for various reasons I decided not to use them.

The [cypress/browsers](https://hub.docker.com/r/cypress/browsers) image would be the first idea, but old chrome version and way too old node version (something didn't compile :shrug:).

[zenika/alpine-chrome](https://hub.docker.com/r/zenika/alpine-chrome) was close to perfect, but there was one dependency missing for using chrome headless with cypress (probably the same mentioned [here](https://github.com/Zenika/alpine-chrome/pull/174)). Could have submitted a PR, but not this time.