FROM node:slim AS builder
# keep all this in one run cmd, because the image won't shrink again after installation (docker layer)
RUN apt-get -qq update && apt-get install -y \
    libgtk2.0-0 libgtk-3-0 libnotify-dev libgconf-2-4 \
    libnss3 libxss1 libasound2 libxtst6 xauth xvfb wget \
    procps openssh-client git \
    && wget --quiet https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && apt-get install -y ./google-chrome-stable_current_amd64.deb \
    && rm -f google-chrome-stable_current_amd64.deb \
    && rm -f /etc/apt/sources.list.d/google-chrome.list \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{apt,cache,log}
RUN npx cypress install